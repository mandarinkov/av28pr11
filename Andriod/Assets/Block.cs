﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Block : MonoBehaviour {
    void OnCollisionEnter2D(Collision2D collisionInfo)
    {
        // Destroy the whole Block
        Destroy(gameObject);
        MainCameraScript.blocks++;
        if (MainCameraScript.blocks==5)
        {
            string sceneName = "scene1";
            SceneManager.LoadScene(sceneName,LoadSceneMode.Single);
        }
    }
}
