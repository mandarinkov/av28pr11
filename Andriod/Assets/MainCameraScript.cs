﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCameraScript : MonoBehaviour
{

    public static int blocks;
    public static int racket = 3;
    public int stepX = 750;
    public int W = 100;
    public int H = 25;
    public static int maxWidth = 600;
    public static int maxHeight = 300;
    public bool btnClicked;
    public bool checkResult = true;
    public int counter;
    public bool isWindowShow = false;
    private static Rect windowRect = new Rect(10,
                           maxHeight / 3 + 25,
                           maxWidth / 3,
                           maxHeight / 3);
    void Start()
    {
        racket = 3;
    }
    void OnGUI()
    {
        maxWidth = Screen.width;
        maxHeight = Screen.height;
        //W = (maxWidth / 3) / 6;
        //H = (maxHeight / 3) / 12;
        GUI.Label(new Rect(maxWidth - stepX, 10, W, H),
            "очки " + blocks);
        GUI.Label(new Rect(maxWidth - stepX, 10+H, W, H),
            "жизни " + racket);
        #region menu
        /*
        //Box
        //Button
        //Toggle
        GUI.Box(new Rect(5, 10, maxWidth / 3 - 10, maxHeight / 3), "Title");
        GUI.BeginGroup(new Rect(5, 10, maxWidth / 3 - 10, maxHeight / 3));
        GUI.Label(new Rect(10, 10, 150, 25), "Кнопку нажали " + counter + " раз");
        btnClicked = GUI.Button(new Rect(10, 10 + 25, 100, 25), "Нажми меня!");
        if (btnClicked)
        {//OnClick
            counter++;
        }
        checkResult = GUI.Toggle(new Rect(10, 60, 125, 25),
            checkResult,
            "show window #0 ?");
        if (checkResult)
        {
            isWindowShow = true;
        }
        else
        {
            isWindowShow = false;
        }
        GUI.EndGroup();

        if (isWindowShow)
        {
            windowRect = GUI.Window(0,//номер этого окна
                windowRect,
                drawWindowContent,
                "Тестовое окно № 0"
                );
        }
        */
        #endregion
    }
    private void drawWindowContent(int idWindow)
    {
        GUI.BeginGroup(new Rect(0, 0, 250, 250));
        GUI.Label(new Rect(10, 10, 100, 25), "label 1");
        GUI.Label(new Rect(10, 35, 200, 25), "Закрыть приложение?");
        bool yes = GUI.Button(new Rect(10, 55, 100, 25), "Yes?");
        bool no = GUI.Button(new Rect(110, 55, 100, 25), "No?");
        if (yes)
        {
            Application.Quit();
        }

        GUI.EndGroup();
        GUI.DragWindow(new Rect(0,0,1200,800));

    }

}
