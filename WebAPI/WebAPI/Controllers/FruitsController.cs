﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UnityEngine;
namespace WebAPI.Controllers
{
    public class FruitsController : ApiController
    {
        static string[] s = { "proj1","proj2" };
        static MyUser MyUser = new MyUser("User1",s);
        static string json = JsonUtility.ToJson(MyUser);
       
        // GET api/name

        public IEnumerable<string> Get()
        {
            return json;
        }

        #region Такая реализация возвращает правильный ответ: 404 - Not Found
        public HttpResponseMessage Get(int id)
        {
            if (id < json.Length)
            {
                return Request.CreateResponse(HttpStatusCode.OK, json[id]);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Item not found"); //Status code 404
            }
        }
        #endregion
    }
    [Serializable]
    class MyUser
    {
        public string UserName;
        public string[] projects;
        public MyUser(string un,string[]pr)
        {
            UserName = un;
            projects = pr;
        }
    }
}
