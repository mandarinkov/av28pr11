﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Text;
using System;
using UnityEngine.UI;
public class ApiHelper : MonoBehaviour
{
    public GameObject[] user;
    // Use this for initialization
    void Start()
    {
        StartCoroutine(getWWW());
       
    }
    #region GET
    IEnumerator getWWW()
    {
        string url = "http://localhost:1497/api/fruits";
        WWW www = new WWW(url);
        while (!www.isDone)
        {
            yield return null;
        }
        if (string.IsNullOrEmpty(www.error))
        {
            Debug.Log(www.text);
            string tmp = "";
            for (int i = 2; i < www.text.Length-2; i++)
            {
                if (www.text[i]=='\\')
                {
                    continue;
                }
                tmp += www.text[i];
            }
            MyUser userA = JsonUtility.FromJson<MyUser>(tmp);
            if (user[0].gameObject.tag == "user1")
            {
                user[0].GetComponentInChildren<Text>().text = userA.UserName;
            }
            if (user[1].gameObject.tag == "user2")
            {
                user[1].GetComponentInChildren<Text>().text = "User2";
            }
            if (user[1].gameObject.tag == "user3")
            {
                user[1].GetComponentInChildren<Text>().text = "User3";
            }
            else
            {
                Debug.Log("There no user");
            }

        }
        else
        {
            Debug.Log(www.error);
        }

    }
    [Serializable]
    class MyUser
    {
        public string UserName;
        public string[] projects;
        public MyUser(string un, string[] pr)
        {
            UserName = un;
            projects = pr;
        }
       
    }
    #endregion
}
